/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage:
       {'homeBg': "url('../src/images/background1.png')",
        'prawntoBg': "url('/prawnto/heroPage_1.png')",
        'carcarBg': "url('/carcar/menu_options.png')",
        'prolistBg': "url('/prolist/login.png')",
        'prolist01': "url('/prolist/login.png')",},

    },
  },
  plugins: [],
}
