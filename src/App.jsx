import './App.css'
import Header from './components/header'
import Home from './components/home'
import About from './components/about'
import Projects from './components/projects'
import Contact from './components/contact'
import { Analytics } from "@vercel/analytics/react"
import Footer from './components/footer'

function App() {

  return (
    <>
    <Header />
    <Home />
    <About />
    <Projects />
    <Contact />
    <Analytics />
    </>
  )
}

export default App
