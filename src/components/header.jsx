import React from "react";
import {Link} from 'react-scroll'
import { motion } from "framer-motion";
import { useState } from "react";
import { Menu, X } from 'lucide-react';


const Header = () => {


    const [isOpen, setIsOpen] = useState(false);
    const Links = () => {
        return(
            <div className="flex flex-col md:flex-row gap-10 pb-10">
                <Link to="hero" spy={true} offset={-100} smooth={true} duration={500} className="hover:text-slate-600 hover:cursor-pointer">home</Link>
                <Link to="about" spy ={true} offset={-100} smooth={true} duration={500} className="hover:text-slate-600 hover:cursor-pointer">about</Link>
                <Link to="projects" spy={true} offset={-79} smooth={true} duration={500} className="hover:text-slate-600 hover:cursor-pointer">projects</Link>
                <Link to="contact" spy={true} offset={-70} smooth={true} duration={500} className="hover:text-slate-600 hover:cursor-pointer">contact</Link>
            </div>
        )
    }
    const Links2 = () => {
        return(
            <div className="flex flex-col md:flex-row gap-10 pb-10">
                <Link to="hero" spy={true} offset={-300} smooth={true} duration={500} className="hover:text-slate-600 hover:cursor-pointer" onClick={toggleNav}>home</Link>
                <Link to="about" spy ={true} offset={-300} smooth={true} duration={500} className="hover:text-slate-600 hover:cursor-pointer" onClick={toggleNav}>about</Link>
                <Link to="projects" spy={true} offset={-300} smooth={true} duration={500} className="hover:text-slate-600 hover:cursor-pointer" onClick={toggleNav}>projects</Link>
                <Link to="contact" spy={true} offset={-300} smooth={true} duration={500} className="hover:text-slate-600 hover:cursor-pointer" onClick={toggleNav}>contact</Link>
            </div>
        )
    }


    const toggleNav = () => {
        setIsOpen(!isOpen);
        }

    return (
        <div className= "z-20 sticky top-0 left-0 min-h-20 w-screen bg-black text-center justify-between items-center">
            <motion.div initial={{ x: 250, opacity: 0 }} animate={{ x: 0, opacity: 1 }} transition={{ delay: .5, duration: .5 }} className="hidden md:flex justify-end text-white text-xl font-medium pr-12 pt-6 ">
                <div className="flex flex-row gap-10">
                <Links />
                </div>
            </motion.div>
            <div className="text-white justify-end text-end pr-10 pt-6 md:hidden">
                <button onClick={toggleNav}>{isOpen ? <X /> : <Menu />}</button>
                </div>
                <div>
                    {isOpen && (<div className="flex flex-col items-center text-white text-xl">
                        <Links2 />
                        </div>)}
                </div>

        </div>
    )
}

export default Header;
