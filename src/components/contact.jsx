import React from "react";

const Contact = () => {

    return(
        <div id="contact" className="relative bg-white min-h-auto w-wcreen p-4 pb-40">
            <div className="text-5xl md:text-5xl font-medium pt-20 text-center">
                <p className="text-slate-600">&lt; contact /&gt;</p>
            </div>
            <div className="flex flex-col pt-20 items-center">
            <form action="https://getform.io/f/jbwllgga" method="POST" className="flex flex-col max-w-[600px] w-full space-y-5 border border-slate-600 p-6 rounded-lg shadow-lg bg-gray-100">
                <input className="border border-slate-600 px-4 py-2 rounded"type="text" placeholder="Name" name="name"/>
                <input className="border border-slate-600 px-4 py-2 rounded" type="text" placeholder="email" name="email"/>
                <textarea className="border border-slate-600 px-4 py-2 rounded" name="message" rows="10" placeholder="Message"></textarea>
                <div className="flex justify-center items-center">
                <div className="border border-slate-600 px-4 rounded-lg text-center py-2 bg-black text-white font-medium hover:bg-gray-600 hover:text-white">
                <button className="">&lt; contact_me /&gt;</button>
                </div>
                </div>
            </form>
            </div>
            <div className="flex items-center justify-center pt-20 space-x-10">
                <a href="https://www.linkedin.com/in/denver-alexander/" target="_blank"><img className="w-10" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/linkedin/linkedin-plain.svg" /></a>
                <a href="https://gitlab.com/denveriv" target="_blank"><img className="w-10" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/gitlab/gitlab-plain.svg" /></a>

            </div>

        </div>
    )
}

export default Contact;
