import React from "react";
import {motion } from 'framer-motion';


const About = () => {
    const aboutVariants = {
        hidden: {
            opacity: 0,
            x: '50'
        },
        visible: {
            opacity: 1,
            x: 0,
            transition: {
                delay: 0.5,
                type: 'spring',
                mass: 0.4,
                damping: 8,
                when: 'beforeChildren',
                staggerChildren: 0.3,
            }
        },
    }

    const containerVariants = {
        hidden: {
            opacity: 0,
            x: 40,
        },
        visible: {
            opacity: 1,
            x: 0,
            type: 'spring',
    }}

    return (
        <>
        <motion.div id="about" className="relative h-auto w-screen bg-white text-slate-600"
        variants={aboutVariants}
        initial="hidden"
        whileInView="visible"
        viewport={{ once: true, }}>
            <h1 className="text-5xl font-medium text-center pt-20">&lt; about /&gt;</h1>
                <div className="flex font-sans text-2xl font-medium justify-center text-center items-center p-8 lg:text-3xl lg:pt-40 pb-32">
                    <motion.div className="container"
                    variants={containerVariants}>
                    <p className="font-normal">I am a Full Stack Engineer! Whether I'm tackling a woodworking project,
                    experimenting with a new hot sauce recipe, or diving into coding your
                    next project, you can count on me to deliver exceptional results. I approach
                    each endeavor with maximum effort and dedication, ensuring that the final
                    product reflects my commitment to excellence.</p>
                    </motion.div>
                </div>
                <div className="bg-gray-600 items-center justify-center">
                <div className="flex flex-wrap px-2 justify-between grid-cols-2 pt-5 space-y-4 md:flex md:flex-row text-black md:pt-20 pb-20 md:space-x-10 items-center lg:justify-center">
                    <motion.img className="h-20 md:h-40" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/tailwindcss/tailwindcss-original.svg"
                    variants={containerVariants}/>
                    <motion.img className="h-20 md:h-40" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/react/react-original-wordmark.svg"
                    variants={containerVariants} />
                    <motion.img className="h-20 md:h-40" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/javascript/javascript-original.svg"
                    variants={containerVariants}/>
                    <motion.img className="h-20 md:h-40" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/python/python-original-wordmark.svg"
                    variants={containerVariants}/>
                    <motion.img className="h-20 md:h-40" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/fastapi/fastapi-original-wordmark.svg"
                    variants={containerVariants}/>
                    <motion.img className="h-20 md:h-40" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/django/django-plain-wordmark.svg"
                    variants={containerVariants}/>
                    <motion.img className="h-20 md:h-40" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/gitlab/gitlab-original-wordmark.svg"
                    variants={containerVariants}/>
                    <motion.img className="h-20 md:h-40" src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/docker/docker-original-wordmark.svg"
                    variants={containerVariants}/>
                </div>
                </div>
            </motion.div>
        </>
    )
}


export default About;
