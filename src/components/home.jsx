import React from "react";
import {Link} from 'react-scroll'
import { motion } from 'framer-motion'


const Home = () => {

    return (
        <div id="hero"className="relative bg-black min-w-screen min-h-screen sm:h-auto">
        <div className="sm:bg-black sm:cover w-screen min-h-screen lg:bg-homeBg bg-cover bg-center bg-fixed">
            <div>
                <h1 className="text-white text-5xl font-sans text-center font-medium pt-10"> &lt; coder /&gt;</h1>
            </div>
            <div className="sm:flex-col justify-between">
            <motion.div initial={{x:-250, opacity: 0}} animate={{x:0, opacity: 1}} transition={{delay: .5, duration: .5}} className="lg:absolute lg:bottom-96 lg:left-10 items-center container">
                <p className="text-white max-w-3xl text-center text-2xl lg:text-left lg:text-2xl p-8">Hello, I am Denver Alexander,
                a Full Stack Engineer in Fredericksburg, VA.
                I am passionate about developing fast,
                responsive, intuitive and dynamic
                applications.</p>
            </motion.div>
            <motion.div initial={{y:'100vh'}} animate={{y: 0}} transition={{ delay: .5, duration: .5}} className="lg:absolute lg:bottom-80 lg:right-80 lg:justify-end flex justify-center pt-8">
            <Link to="contact" spy={true} offset={-100} smooth={true} duration={500}><p className="text-white text-2xl font-medium border px-6 py-3 rounded-lg hover:text-black hover:bg-white hover:cursor-pointer active:bg-gray-500 active:text-white transition-colors duration-300">contact_me</p></Link>
            </motion.div>
            </div>
        </div>
        </div>
    )
}

export default Home;
