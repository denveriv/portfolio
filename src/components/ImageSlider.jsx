import React from "react";
import { useState } from "react";

const ImageSlider = ({slides}) => {

    const [currentIndex, setCurrentIndex] = useState(0);

    const goToPrevious = () => {
        const isFirstSlide = currentIndex === 0;
        const newIndex = isFirstSlide ? slides.length -1 : currentIndex -1;
        setCurrentIndex(newIndex);
    }


    const goToNext = () => {
        const isLastSlide = currentIndex === slides.length -1;
        const newIndex = isLastSlide ? 0 : currentIndex + 1;
        setCurrentIndex(newIndex);
    }

    return(
        <div className="relative h-full ">
            <div className="w-full h-full rounded bg-contain bg-no-repeat bg-center" style={{backgroundImage: `url(${slides[currentIndex].url})`}}>
                <div onClick={goToPrevious}className="absolute top-1/2 left-[32px] bg-gray-300/30 p-2 rounded-full cursor-pointer hover:bg-gray-300/75 transition-all duration-500">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6"><path strokeLinecap="round" strokeLinejoin="round" d="M15.75 19.5 8.25 12l7.5-7.5" />
                    </svg>
                </div>
                <div className= "absolute top-1/2 right-[32px]">
                    <div onClick={goToNext} className=" p-2 bg-gray-300/30 rounded-full cursor-pointer hover:bg-gray-300/75 transition-all duration-500">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6"><path strokeLinecap="round" strokeLinejoin="round" d="m8.25 4.5 7.5 7.5-7.5 7.5" />
                    </svg>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default ImageSlider;
