import React from "react";
import { useState } from "react";
import Prawnto from "./Modals/prawnto";
import CarCar from "./Modals/carcar";
import Prolist from "./Modals/prolist";
import {motion} from 'framer-motion'



const Projects = () => {

    const [open, setOpen] = useState(false)
    const [open2, setOpen2] = useState(false)
    const [open3, setOpen3] = useState(false)

    const project2Variants = {
        hidden: {
            opacity: 0,
            y: 200,
        },
        visible: {
            opacity: 1,
            y: 0,
            transition: {
                delay: .8,
                duration: 0.5
            }
        }
    }

    const project3Variants = {
        hidden: {
            opacity: 0,
            y: 200,
        },
        visible: {
            opacity: 1,
            y: 0,
            transition: {
                delay: 1.1,
                duration: 0.5
            }
        }
    }
    const projectVariants = {
        hidden: {
            opacity: 0,
            y: 200
        },
        visible: {
            opacity: 1,
            y: 0,
            transition: {
                delay: 0.5,
                duration: 0.5,
            }
        }

    }

    return(
        <div id="projects" className="relative bg-black min-h-screen w-screen pb-20">
            <div>
                <h1 className="text-white text-5xl lg:text-5xl font-sans text-center font-medium pt-20"> &lt; projects /&gt;</h1>
            </div>
            <div className="flex flex-col items-center justify-center pt-40 space-y-10 md:space-y-0 md:flex-row md:justify-center md:space-x-10">
                <motion.div className="bg-prawntoBg bg-cover bg-left bg-no-repeat h-60 w-[350px] rounded-xl"
                    variants={projectVariants}
                    initial="hidden"
                    whileInView="visible"
                    viewport={{once:true,}}>
                    <div className="bg-white w-full h-full opacity-0 rounded-xl hover:opacity-100 transition-all duration-500">
                        <div className="text-slate-600 items-center text-center text-2xl font-bold pt-5">
                            <p>Prawnto</p>
                        </div>
                        <div className="text-center text-slate-600">
                            <p>An exam creation and sharing application.</p>
                        </div>
                        <div className="flex justify-center space-x-5 pt-10">
                            <div className="text-xl font-medium border border-slate-600 px-4 py-1 rounded-lg cursor-pointer hover:bg-black hover:text-white" onClick={() => setOpen(true)}>
                                <p>Demo</p>
                            </div>
                            <a href="https://gitlab.com/mantis-shrimp1/Prawnto" target="_blank" rel="noopener"><div className="text-xl font-medium border border-slate-600 px-4 py-1 rounded-lg cursor-pointer hover:bg-black hover:text-white">
                                <p>GitLab</p>
                            </div></a>
                        </div>

                    </div>
                </motion.div>
                <motion.div className="bg-carcarBg bg-cover bg-left bg-no-repeat h-60 w-[350px] rounded-xl"
                variants={project2Variants}
                initial="hidden"
                whileInView="visible"
                viewport={{once:true,}}>
                    <div className="bg-white w-full h-full opacity-0 rounded-xl hover:opacity-100 transition-all duration-500">
                        <div className="text-slate-600 items-center text-center text-2xl font-bold pt-5">
                            <p>CarCar</p>
                        </div>
                        <div className="text-center text-slate-600">
                            <p>A car sales, and service application.</p>
                        </div>
                        <div className="flex justify-center space-x-5 pt-10">
                            <div className="text-xl font-medium border border-slate-600 px-4 py-1 rounded-lg cursor-pointer hover:bg-black hover:text-white" onClick={() => setOpen2(true)}>
                                <p>Demo</p>
                            </div>
                            <a href="https://gitlab.com/denveriv/car-car" target="_blank" rel="noopener"><div className="text-xl font-medium border border-slate-600 px-4 py-1 rounded-lg cursor-pointer hover:bg-black hover:text-white">
                                <p>GitLab</p>
                            </div></a>
                        </div>

                    </div>

                </motion.div>
                <motion.div  className="bg-prolistBg bg-cover bg-center bg-no-repeat h-60 w-[350px] rounded-xl"
                variants={project3Variants}
                initial="hidden"
                whileInView="visible"
                viewport={{once:true,}}>
                    <div className="bg-white w-full h-full opacity-0 rounded-xl hover:opacity-100 transition-all duration-500">
                        <div className="text-slate-600 items-center text-center text-2xl font-bold pt-5">
                            <p>Prolist</p>
                        </div>
                        <div className="text-center text-slate-600">
                            <p>A project task management application.</p>
                        </div>
                        <div className="flex justify-center space-x-5 pt-10">
                            <div className="text-xl font-medium border border-slate-600 px-4 py-1 rounded-lg cursor-pointer hover:bg-black hover:text-white"onClick={() => setOpen3(true)}>
                                <p>Demo</p>
                            </div>
                            <a href="https://gitlab.com/denveriv/project-alpha-apr" target="_blank" rel="noopener"><div className="text-xl font-medium border border-slate-600 px-4 py-1 rounded-lg cursor-pointer hover:bg-black hover:text-white">
                                <p>GitLab</p>
                            </div></a>
                        </div>

                    </div>

                </motion.div>
            </div>
            <Prawnto open={open} onClose={()=> setOpen(false)}/>
            <CarCar open={open2} onClose={()=> setOpen2(false)}/>
            <Prolist open={open3} onClose={()=> setOpen3(false)}/>
        </div>
    )
}


export default Projects;
