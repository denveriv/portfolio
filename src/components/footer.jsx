import React from "react";

const Footer = () => {

    return(
        <div className="relative bg-gray-600 w-screen h-40">
            <div className="flex flex-row text-white text-2xl items-center justify-center text-center pt-5">
                <p>&lt; socials /&gt;</p>
            </div>
        </div>

    )
}


export default Footer;
