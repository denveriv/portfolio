import React from "react";
import ImageSlider from "../ImageSlider"

const Prolist = ({open, onClose, children}) => {

    const prolistSlides = [
        { url: "/prolist/login.png", title: ""},
        { url: "/prolist/create_project.png", title: ""},
        { url: "/prolist/projects_page.png", title: ""},
        { url: "/prolist/create_task.png", title: ""},
        { url: "/prolist/tasks_page.png", title: ""},
        { url: "/prolist/edit_task.png", title: ""},
        { url: "/prolist/notes_page.png", title: ""},
        { url: "/prolist/gnatt_chart.png", title: ""},
        { url: "/prolist/delete_task.png", title: ""},
    ];


    return(
        <div onClick={onClose} className={`fixed inset-0 flex justify-center items-center transition-colors ${open ? "visible bg-black/25 backdrop-blur" : "invisible"} transition-all duration-150`}>
            <div onClick ={e=> e.stopPropagation() } className="h-auto w-auto bg-white shadow-lg border border-slate-600 rounded-xl p-6 items-center text-center">
                <div className="w-[375px] h-[210px] margin-auto lg:w-[1250px] lg:h-[650px]">
                    <ImageSlider slides={prolistSlides} />
                </div>
            </div>

        </div>
    )
}

export default Prolist;
