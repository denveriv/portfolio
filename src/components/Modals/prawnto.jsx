import React from "react";
import ImageSlider from "../ImageSlider";

const Prawnto = ({open, onClose, children}) => {

    const prawntoSlides = [
        { url: "/prawnto/heroPage_1.png", title: "Main Page"},
        { url: "/prawnto/signup.png", title: "Signup Page"},
        { url: "/prawnto/login.png", title: "Login Page"},
        { url: "/prawnto/menu.png", title: "Dropdown Menu"},
        { url: "/prawnto/explore.png", title: "Explore available exams"},
        { url: "/prawnto/makeexam.png", title: "Make an exam"},
        { url: "/prawnto/createquestion.png", title: "Create Questions"},
        { url: "/prawnto/myexams.png", title: "View all exams you made"},
        { url: "/prawnto/gradebook.png", title: "Gradebook for all attempts"},
        { url: "/prawnto/takeexam.png", title: "Take an exam"},
        { url: "/prawnto/examattempts.png", title: "All exam attempts made"},
    ];


    return(
        <div onClick={onClose} className={`fixed inset-0 flex justify-center items-center transition-colors ${open ? "visible bg-black/25 backdrop-blur" : "invisible"} transition-all duration-150`}>
            <div onClick ={e=> e.stopPropagation() } className="h-auto w-auto bg-white shadow-lg border border-slate-600 rounded-xl p-6 items-center text-center">
                <div className="w-[375px] h-[210px] margin-auto lg:w-[1250px] lg:h-[650px]">
                    <ImageSlider slides={prawntoSlides} />
                </div>
            </div>

        </div>
    )
}

export default Prawnto;
