import React from "react";
import ImageSlider from "../ImageSlider";

const Prawnto = ({open, onClose, children}) => {

    const carcarSlides = [
        { url: "/carcar/main_page.png", title: ""},
        { url: "/carcar/menu_options.png", title: ""},
        { url: "/carcar/create_tech.png", title: ""},
        { url: "/carcar/tech_list.png", title: ""},
        { url: "/carcar/create_appointment.png", title: ""},
        { url: "/carcar/appointments.png", title: ""},
        { url: "/carcar/serviceHistory.png", title: ""},
        { url: "/carcar/search.png", title: ""},
    ];


    return(
        <div onClick={onClose} className={`fixed inset-0 flex justify-center items-center transition-colors ${open ? "visible bg-black/25 backdrop-blur" : "invisible"} transition-all duration-150`}>
            <div onClick ={e=> e.stopPropagation() } className="h-auto w-auto bg-white shadow-lg border border-slate-600 rounded-xl p-6 items-center text-center">
                <div className="w-[375px] h-[210px] margin-auto lg:w-[1250px] lg:h-[650px]">
                    <ImageSlider slides={carcarSlides} />
                </div>
            </div>

        </div>
    )
}

export default Prawnto;
